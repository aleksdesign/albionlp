<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Albion Online - Founderpacks</title>
        <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css"/>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'/>
    <link rel="stylesheet" href="/assets/css/theme.css"/>
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-fluid main">
    <div class="background">
      <div class="fullscreen-bg top">
        <video autoplay  poster="/assets/img/image-top-bg.jpg" id="bgvid" loop>
        <source src="/assets/video/bg_low.webm" type="video/webm">
        <source src="/assets/video/bg_low.mp4" type="video/mp4">
        </video>
    </div>
      <img class="imgtop" src="/assets/img/image-top-bg.jpg" alt="Albion Online Landscape Background Top">
    </div>
    <header>
      <a href="https://albiononline.com" class="logo"><img src="/assets/img/logo_black_small.png" alt="Albion Online Logo"></a>
    </header>
    </div>

  <div class="container">
    <div class="row">
       <div class="col-md-12">
         <h1>Станьте основателем</h1>
         <h2>Исследуйте новый мир Альбиона</h2>
         <!--Sale Content Tabs-->
          <div class="panel with-nav-tabs panel-albion packs">
             <div class="panel-heading">
                     <ul class="nav nav-tabs">
                       <li class="legendary active"><a href="#tab1default" data-toggle="tab" data-class="mage">Легенда</a></li>
                       <li class="epic"><a href="#tab2default" data-toggle="tab" data-class="warrior">Герой</a></li>
                         <li class="veteran"><a href="#tab3default" data-toggle="tab" data-class="thief">Ветеран</a></li>
                     </ul>
             </div>
           </div>
         <!--Sale Content Tabs-->
       </div>
    </div>
  </div>

  <div class="container-fluid packs">
      <div class="row contentbg mage">
        <div class="col-lg-8 col-lg-offset-2 col-md-offset-0">
        <div class="panel with-nav-tabs panel-albion">
          <!--Sale Content-->
            <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane legendary fade active in" id="tab1default">
                    <!--Price Block + Buy Now-->
                    <div class="col-md-3">
                      <div class="priceblock">
                        <div class="icon"><img src="/assets/img/tier-shield-legendary.png" alt="Legendary Tier Shield" /></div>
                        <h3 class="name">Пакет основателя «Легенда»</h3>
                        <span class="price">$99.95</span>
                      </div>
                    <a class="button" href="https://albiononline.com/ru/shop/checkout/begin?product=legendary-founder">Купить</a>
                  </div>
                  <!--Item Listing-->
                  <div class="col-md-9">
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-beta check premium">
                            <span class="octicon octicon-check"></span>
                            Уровень доступа 1 к финальной стадии бета-тестирования
                          </li>
                          <li class="list-group-item item-gold check gold">
                              <span class="octicon octicon-check"></span>
                              12,000 золотых (~50 $)
                        </li>
                          <li class="list-group-item item-premium check items">
                            <span class="octicon octicon-check"></span>
                            Премиум-статус на 90 дней
                          </li>
                          <li class="list-group-item item-founder check premium">
                            <span class="octicon octicon-check"></span>
                            Сертификат основателя
                          </li>
                          <li class="list-group-item item-tag check gold">
                              <span class="octicon octicon-check"></span>
                              Золотой жетон основателя
                        </li>
                          <li class="list-group-item item-avatar check items">
                            <span class="octicon octicon-check"></span>
                            Золотой аватар и венец
                          </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-equipment check house">
                            <span class="octicon octicon-check"></span>
                            Снаряжение легендарного исследователя
                        </li>
                          <li class="list-group-item item-lookingglass check oxe">
                            <span class="octicon octicon-check"></span>
                            Подзорная труба исследователя
                        </li>
                          <li class="list-group-item item-house check horse">
                            <span class="octicon octicon-check"></span>
                            Дом исследователя
                        </li>
                        <li class="list-group-item item-horse check house">
                          <span class="octicon octicon-check"></span>
                          Конь исследователя
                      </li>
                        <li class="list-group-item item-oxe check oxe">
                          <span class="octicon octicon-check"></span>
                          Бык исследователя
                      </li>
                        <li class="list-group-item item-statue check horse">
                          <span class="octicon octicon-check"></span>
                          Статуя исследователя
                      </li>
                      </ul>
                    </div>
                  </div>


                  </div>
                  <div class="tab-pane epic fade" id="tab2default">
                    <!--Price Block + Buy Now-->
                    <div class="col-md-3">
                      <div class="priceblock">
                        <div class="icon"><img src="/assets/img/tier-shield-epic.png" alt="Epic Tier Shield" /></div>
                        <h3 class="name">Пакет основателя «Герой»</h3>
                        <span class="price">$49.95</span>
                      </div>
                    <a class="button" href="https://albiononline.com/ru/shop/checkout/begin?product=epic-founder">Купить</a>
                  </div>
                  <!--Item Listing-->
                  <div class="col-md-9">
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-beta-two check premium">
                            <span class="octicon octicon-check"></span>
                            ровень доступа 2 к финальной стадии бета-тестирования
                          </li>
                          <li class="list-group-item item-gold check gold">
                              <span class="octicon octicon-check"></span>
                              4,500 золотых (~20 $)
                        </li>
                          <li class="list-group-item item-premium check items">
                            <span class="octicon octicon-check"></span>
                            Премиум-статус на 60 дней
                          </li>
                          <li class="list-group-item item-founder check premium">
                            <span class="octicon octicon-check"></span>
                            Сертификат основателя
                          </li>
                          <li class="list-group-item item-tag check gold">
                              <span class="octicon octicon-check"></span>
                              Серебряный жетон основателя
                        </li>
                          <li class="list-group-item item-avatar check items">
                            <span class="octicon octicon-check"></span>
                            Серебряный аватар и венец
                          </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-equipment check house">
                            <span class="octicon octicon-check"></span>
                            Снаряжение героического исследователя
                        </li>
                          <li class="list-group-item item-lookingglass check oxe">
                            <span class="octicon octicon-check"></span>
                            Подзорная труба исследователя
                        </li>
                          <li class="list-group-item item-house nocheck horse">
                            <span class="octicon octicon-x"></span>
                            Дом исследователя
                        </li>
                        <li class="list-group-item item-horse nocheck house">
                          <span class="octicon octicon-x"></span>
                          Конь исследователя
                      </li>
                        <li class="list-group-item item-oxe nocheck oxe">
                          <span class="octicon octicon-x"></span>
                          Бык исследователя
                      </li>
                        <li class="list-group-item item-statue nocheck horse">
                          <span class="octicon octicon-x"></span>
                          Статуя исследователя
                      </li>
                      </ul>
                    </div>
                  </div>
                  </div>
                  <div class="tab-pane fade veteran" id="tab3default">
                      <!--Price Block + Buy Now-->
                      <div class="col-md-3">
                        <div class="priceblock">
                          <div class="icon"><img src="/assets/img/tier-shield-veteran.png" alt="Veteran Tier Shield" /></div>
                          <h3 class="name">Пакет основателя «Ветеран»</h3>
                          <span class="price">$29.95</span>
                        </div>
                      <a class="button" href="https://albiononline.com/ru/shop/checkout/begin?product=veteran-founder">Купить</a>
                    </div>
                    <!--Item Listing-->
                    <div class="col-md-9">
                      <div class="col-sm-6">
                        <ul class="list-group itemlist">
                            <li class="list-group-item item-beta-three check premium">
                              <span class="octicon octicon-check"></span>
                              ровень доступа 3 к финальной стадии бета-тестирования
                            </li>
                            <li class="list-group-item item-gold check gold">
                                <span class="octicon octicon-check"></span>
                                2,000 золотых (~10 $)
                          </li>
                            <li class="list-group-item item-premium check items">
                              <span class="octicon octicon-check"></span>
                              Премиум-статус на 30 дней
                            </li>
                            <li class="list-group-item item-founder check premium">
                              <span class="octicon octicon-check"></span>
                              Сертификат основателя
                            </li>
                            <li class="list-group-item item-tag nocheck gold">
                                <span class="octicon octicon-x"></span>
                                Жетон основателя
                          </li>
                            <li class="list-group-item item-avatar nocheck items">
                              <span class="octicon octicon-x"></span>
                              Аватар и венец
                            </li>
                        </ul>
                      </div>
                      <div class="col-sm-6">
                        <ul class="list-group itemlist">
                            <li class="list-group-item item-equipment nocheck house">
                              <span class="octicon octicon-x"></span>
                              Снаряжение исследователя
                          </li>
                            <li class="list-group-item item-lookingglass nocheck oxe">
                              <span class="octicon octicon-x"></span>
                              Подзорная труба исследователя
                          </li>
                            <li class="list-group-item item-house nocheck horse">
                              <span class="octicon octicon-x"></span>
                              Дом исследователя
                          </li>
                          <li class="list-group-item item-horse nocheck house">
                            <span class="octicon octicon-x"></span>
                            Конь исследователя
                        </li>
                          <li class="list-group-item item-oxe nocheck oxe">
                            <span class="octicon octicon-x"></span>
                            Бык исследователя
                        </li>
                          <li class="list-group-item item-statue nocheck horse">
                            <span class="octicon octicon-x"></span>
                            Статуя исследователя
                        </li>
                        </ul>
                      </div>
                    </div>
                    </div>



                </div>
            </div>
          <!--Sale Content-->
        </div>
        </div>
    </div>
  </div>


  <div class="footer">
    <div class="container">
      <div class="row">
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">ЧТО ТАКОЕ ALBION ONLINE?</h6>
              <ul class="footer-menu">
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/home#fighting">СРАЖЕНИЯ</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/home#economy">ЭКОНОМИКА</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/home#housing">ВЛАДЕНИЯ</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/home#character">ПЕРСОНАЖИ</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/home#world">МИР</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/home#devices">ПЛАТФОРМЫ</a></li>
              </ul>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">КОМПАНИЯ</h6>
              <ul class="footer-menu">
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/jobs">ВАКАНСИИ</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/imprint">О КОМПАНИИ</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/privacy_policy">ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/ru/terms_and_conditions">ПРАВИЛА И УСЛОВИЯ</a></li>
              </ul>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">ПОДПИШИТЕСЬ НА НАС</h6>
              <p>
                  <a href="https://twitter.com/albiononline/"><img class="social-link" src="//assets.albiononline.com/assets/images/social/twitter.jpg?cb=75" alt="Twitter"></a>
                  <a href="https://www.facebook.com/albiononline"><img class="social-link" src="//assets.albiononline.com/assets/images/social/facebook.jpg?cb=75" alt="Facebook"></a>
                  <a href="https://www.youtube.com/channel/UC9gij-VpDFWaaqn1zLM4TEQ"><img class="social-link" src="//assets.albiononline.com/assets/images/social/youtube.jpg?cb=75" alt="Youtube"></a>
                  <a href="http://albiononline.gamepedia.com/Albion_Online_Wiki"><img class="social-link" src="//assets.albiononline.com/assets/images/social/gamepedia.jpg?cb=75" alt="Gamepedia"></a>
                  <a href="https://www.reddit.com/r/albiononline"><img class="social-link" src="//assets.albiononline.com/assets/images/social/reddit.jpg?cb=75" alt="Reddit"></a>
              </p>
          </div>
      </div>
      <div class="row">
          <div class="col-sm-12 medium-12 large-12 columns">
              <p class="text-center text-uppercase"><small>© SANDBOX INTERACTIVE GMBH. 2015. ALBION ONLINE ЯВЛЯЕТСЯ ЗАРЕГИСТРИРОВАННЫМ ТОВАРНЫМ ЗНАКОМ В ГЕРМАНИИ И/ИЛИ ДРУГИХ СТРАНАХ. | КОНТАКТ ДЛЯ ПРЕССЫ: PRESS@ALBIONONLINE.COM</small></p>
          </div>
      </div>
    </div>
  </div>



  <div id="item-beta-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>Уровень доступа 1 к финальной стадии бета-тестирования (начинется 1-го августа, 2016-го)<br />Уровень доступа к релизу "Легенда"</p>
    </div>
  </div>

  <div id="item-beta-two-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>ровень доступа 2 к финальной стадии бета-тестирования (начинется 2-го августа, 2016-го)<br />Уровень доступа к релизу "Герой"</p>
    </div>
  </div>

  <div id="item-beta-three-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>Доступ 3 к финальной стадии бета-тестирования (начинется 3-го августа, 2016-го)<br />Уровень доступа к релизу "Ветеран"</p>
    </div>
  </div>

  <div id="item-gold-content" class="hidden">
    <div>
      <img src="/assets/img/icon_gold.png" alt="Gold Icon">
      <p>Вы получите золотые монеты, которые сможете потратить в игровом мире.</p>
    </div>
  </div>


  <div id="item-premium-content" class="hidden">
    <div>
      <img src="/assets/img/icon_premium.png" alt="Premium Icon">
      <p>Премиум-персонажи зарабатывают дополнительное серебро при победе над монстрами, быстрее развиваются и могут получать другие приятные сюрпризы.</p>
    </div>
  </div>

  <div id="item-founder-content" class="hidden">
    <div>
      <img src="/assets/img/icon_founders-certificate.png" alt="Founder Icon">
      <p>Пусть все узнают, что вы одним из первых ступили в мир Альбиона. Сертификат можно повесить на стену своего дома и показывать гостям!</p>
    </div>
  </div>

  <div id="item-tag-content" class="hidden">
    <div>
      <img src="/assets/img/icon_tag.png" alt="Tag Icon">
      <p>Имя вашего персонажа будет написано особым шрифтом, и его первая буква будет выгравирована на жетоне с золотой окантовкой.</p>
    </div>
  </div>

  <div id="item-avatar-content" class="hidden">
    <div>
      <img src="/assets/img/icon_avatar.png" alt="Avatar Icon">
      <p>Венец на аватаре с благородным портретом прекрасно дополняет жетон. Этот аватар обязательно заметят другие игроки, и он придаст вашему персонажу поистине королевский вид.</p>
    </div>
  </div>

  <div id="item-equipment-content" class="hidden">
    <div>
      <img src="/assets/img/icon_items.png" alt="Items Icon">
      <p>Шляпа, туника, сапоги и развевающийся плащ за спиной (только для пакета «Легенда») – вот образ истинного основателя. Немногим игрокам выпадет честь носить эти предметы. Хотите стать одним из них? Предметы могут быть повреждены или украдены.</p>
    </div>
  </div>

  <div id="item-lookingglass-content" class="hidden">
    <div>
      <img src="/assets/img/icon_looking-glass.png" alt="Glas Icon">
      <p>Ни один настоящий первооткрыватель не обойдется без такой трубы. Поместите ее в своем игровом доме и любуйтесь красотами Альбиона. Предмет может быть поврежден или украден.</p>
    </div>
  </div>

  <div id="item-house-content" class="hidden">
    <div>
      <img src="/assets/img/icon_house.png" alt="House Icon">
      <p>Такой дом доступен только легендарным основателям. Он выглядит как перевернутый корабль и отражает страсть его владельца к приключениям и новым открытиям.</p>
    </div>
  </div>

  <div id="item-horse-content" class="hidden">
    <div>
      <img src="/assets/img/icon_horse.png" alt="Horse Icon">
      <p>Этот конь в легкой броне поможет вам перемещаться по миру Альбиона быстрее. Он повышает уровень защиты персонажа, а также его грузоподъемность. Животное может быть убито или украдено.</p>
    </div>
  </div>

  <div id="item-oxe-content" class="hidden">
    <div>
      <img src="/assets/img/icon_ox.png" alt="Ox Icon">
      <p>Этот медленный, но мощный зверь позволит перевозить по Альбиону больше товаров. В мире, где торговля решает все, этот бык станет очень хорошим преимуществом. Животное может быть убито или украдено.</p>
    </div>
  </div>

  <div id="item-statue-content" class="hidden">
    <div>
      <img src="/assets/img/icon_statue.png" alt="Statue Icon">
      <p>Поставьте эту огромную статую в своем поселении, и все соседи обзавидуются. Что может быть приятнее? Предмет может быть поврежден или украден.</p>
    </div>
  </div>

    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      var vid = document.getElementById("bgvid");
      vid.addEventListener('ended', function()
      {
      vid.pause();
      // to capture IE10
      vidFade();
      });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.nav a').click(function(){
        var $contentbg = $('.contentbg'),
            newClass = $(this).attr('data-class');

        $contentbg.removeClass('warrior mage thief');
        $contentbg.addClass(newClass);
      });
    });
    </script>

    <script type="text/javascript">
    var popover = function(sel) {
      $("." + sel).popover({
        html: true,
        trigger: 'hover',
        content: function() {
          return $("#" + sel + "-content").html();
        }
      });
    };

    popover("item-beta");
    popover("item-beta-two");
    popover("item-beta-three");
    popover("item-gold");
    popover("item-premium");
    popover("item-founder");
    popover("item-tag");
    popover("item-avatar");
    popover("item-equipment");
    popover("item-lookingglass");
    popover("item-house");
    popover("item-horse");
    popover("item-oxe");
    popover("item-statue");
    </script>
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TV57XR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TV57XR');</script>
<!-- End Google Tag Manager -->
  </body>
</html>
