var gulp         = require('gulp');
var plumber      = require('gulp-plumber');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

var webserver    = require('gulp-webserver');


var sourcePaths = {
  styles: ['styles/**/*.scss']
};

var distPaths = {
  styles: 'assets/css'
};

// Compile scss files to css
gulp.task('sass', function () {
  gulp.src( sourcePaths.styles )
    .pipe(plumber())
    .pipe(autoprefixer({
      browsers: ['> 2%', 'IE 8'],
      cascade: true
    }))
    .pipe(sass())
    .pipe(gulp.dest( distPaths.styles ));
});

gulp.task('watch', function() {
  gulp.watch(sourcePaths.styles, ['sass']);
});

// gulp.task('webserver', function() {
//   gulp.src('.')
//     .pipe(webserver({
//       livereload: true,
//       directoryListing: {
//       enable: true,
//       path: '.'
//       },
//       open: true
//     }));
// });

gulp.task('build', ['sass']);

gulp.task('default', ['build', 'watch']);
