<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Albion Online - Founderpacks</title>
        <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css"/>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'/>
    <link rel="stylesheet" href="/assets/css/theme.css"/>
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-fluid main">
    <div class="background">
      <div class="fullscreen-bg top">
        <video autoplay  poster="/assets/img/image-top-bg.jpg" id="bgvid" loop>
        <source src="/assets/video/bg_low.webm" type="video/webm">
        <source src="/assets/video/bg_low.mp4" type="video/mp4">
        </video>
    </div>
      <img class="imgtop" src="/assets/img/image-top-bg.jpg" alt="Albion Online Landscape Background Top">
    </div>
    <header>
      <a href="https://albiononline.com" class="logo"><img src="/assets/img/logo_black_small.png" alt="Albion Online Logo"></a>
    </header>
    </div>

  <div class="container">
    <div class="row">
       <div class="col-md-12">
         <h1>Zostań Założycielem</h1>
         <h2>Odkryj nowy świat Albionu</h2>
         <!--Sale Content Tabs-->
          <div class="panel with-nav-tabs panel-albion packs">
             <div class="panel-heading">
                     <ul class="nav nav-tabs">
                       <li class="legendary active"><a href="#tab1default" data-toggle="tab" data-class="mage">Legendarny</a></li>
                       <li class="epic"><a href="#tab2default" data-toggle="tab" data-class="warrior">Epicki</a></li>
                         <li class="veteran"><a href="#tab3default" data-toggle="tab" data-class="thief">Weteran</a></li>
                     </ul>
             </div>
           </div>
         <!--Sale Content Tabs-->
       </div>
    </div>
  </div>

  <div class="container-fluid packs">
      <div class="row contentbg mage">
        <div class="col-lg-8 col-lg-offset-2 col-md-offset-0">
        <div class="panel with-nav-tabs panel-albion">
          <!--Sale Content-->
            <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane legendary fade active in" id="tab1default">
                    <!--Price Block + Buy Now-->
                    <div class="col-md-3">
                      <div class="priceblock">
                        <div class="icon"><img src="/assets/img/tier-shield-legendary.png" alt="Legendary Tier Shield" /></div>
                        <h3 class="name">Legendarny Pakiet</h3>
                        <span class="price">$99.95</span>
                      </div>
                    <a class="button" href="https://albiononline.com/pl/shop/checkout/begin?product=legendary-founder">Kup teraz!</a>
                  </div>
                  <!--Item Listing-->
                  <div class="col-md-9">
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-beta check premium">
                            <span class="octicon octicon-check"></span>
                            Dostęp Poziomu 1 do Ostatecznej Bety
                          </li>
                          <li class="list-group-item item-gold check gold">
                              <span class="octicon octicon-check"></span>
                              12,000 złota (~50 $)
                        </li>
                          <li class="list-group-item item-premium check items">
                            <span class="octicon octicon-check"></span>
                            Status premium na 90 dni
                          </li>
                          <li class="list-group-item item-founder check premium">
                            <span class="octicon octicon-check"></span>
                            Certyfikat Założyciela
                          </li>
                          <li class="list-group-item item-tag check gold">
                              <span class="octicon octicon-check"></span>
                              Złoty emblemat Założyciela
                        </li>
                          <li class="list-group-item item-avatar check items">
                            <span class="octicon octicon-check"></span>
                            Złoty awatar i wieniec
                          </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-equipment check house">
                            <span class="octicon octicon-check"></span>
                            Wyposażenie Legendarnego Odkrywcy
                        </li>
                          <li class="list-group-item item-lookingglass check oxe">
                            <span class="octicon octicon-check"></span>
                            Luneta Odkrywcy
                        </li>
                          <li class="list-group-item item-house check horse">
                            <span class="octicon octicon-check"></span>
                            Dom Odkrywcy
                        </li>
                        <li class="list-group-item item-horse check house">
                          <span class="octicon octicon-check"></span>
                          Koń Odkrywcy
                      </li>
                        <li class="list-group-item item-oxe check oxe">
                          <span class="octicon octicon-check"></span>
                          Wół Odkrywcy
                      </li>
                        <li class="list-group-item item-statue check horse">
                          <span class="octicon octicon-check"></span>
                          Posąg Odkrywcy
                      </li>
                      </ul>
                    </div>
                  </div>


                  </div>
                  <div class="tab-pane epic fade" id="tab2default">
                    <!--Price Block + Buy Now-->
                    <div class="col-md-3">
                      <div class="priceblock">
                        <div class="icon"><img src="/assets/img/tier-shield-epic.png" alt="Epic Tier Shield" /></div>
                        <h3 class="name">Epicki Pakiet</h3>
                        <span class="price">$49.95</span>
                      </div>
                    <a class="button" href="https://albiononline.com/pl/shop/checkout/begin?product=epic-founder">Kup teraz!</a>
                  </div>
                  <!--Item Listing-->
                  <div class="col-md-9">
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-beta-two check premium">
                            <span class="octicon octicon-check"></span>
                            Dostęp Poziomu 2 do Ostatecznej Bety
                          </li>
                          <li class="list-group-item item-gold check gold">
                              <span class="octicon octicon-check"></span>
                              4,500 złota (~20 $)
                        </li>
                          <li class="list-group-item item-premium check items">
                            <span class="octicon octicon-check"></span>
                            Status premium na 60 dni
                          </li>
                          <li class="list-group-item item-founder check premium">
                            <span class="octicon octicon-check"></span>
                            Certyfikat Założyciela
                          </li>
                          <li class="list-group-item item-tag check gold">
                              <span class="octicon octicon-check"></span>
                              Srebrny emblemat Założyciela
                        </li>
                          <li class="list-group-item item-avatar check items">
                            <span class="octicon octicon-check"></span>
                            Srebrny awatar i wieniec
                          </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-equipment check house">
                            <span class="octicon octicon-check"></span>
                            Wyposażenie Heroicznego Odkrywcy
                        </li>
                          <li class="list-group-item item-lookingglass check oxe">
                            <span class="octicon octicon-check"></span>
                            Luneta Odkrywcy
                        </li>
                          <li class="list-group-item item-house nocheck horse">
                            <span class="octicon octicon-x"></span>
                            Dom Odkrywcy
                        </li>
                        <li class="list-group-item item-horse nocheck house">
                          <span class="octicon octicon-x"></span>
                          Koń Odkrywcy
                      </li>
                        <li class="list-group-item item-oxe nocheck oxe">
                          <span class="octicon octicon-x"></span>
                          Wół Odkrywcy
                      </li>
                        <li class="list-group-item item-statue nocheck horse">
                          <span class="octicon octicon-x"></span>
                          Posąg Odkrywcy
                      </li>
                      </ul>
                    </div>
                  </div>
                  </div>
                  <div class="tab-pane fade veteran" id="tab3default">
                      <!--Price Block + Buy Now-->
                      <div class="col-md-3">
                        <div class="priceblock">
                          <div class="icon"><img src="/assets/img/tier-shield-veteran.png" alt="Veteran Tier Shield" /></div>
                          <h3 class="name">Pakiet Weterana</h3>
                          <span class="price">$29.95</span>
                        </div>
                      <a class="button" href="https://albiononline.com/pl/shop/checkout/begin?product=veteran-founder">Kup teraz!</a>
                    </div>
                    <!--Item Listing-->
                    <div class="col-md-9">
                      <div class="col-sm-6">
                        <ul class="list-group itemlist">
                            <li class="list-group-item item-beta-three check premium">
                              <span class="octicon octicon-check"></span>
                              Dostęp do Ostatecznej Bety
                            </li>
                            <li class="list-group-item item-gold check gold">
                                <span class="octicon octicon-check"></span>
                                2,000 złota (~10 $)
                          </li>
                            <li class="list-group-item item-premium check items">
                              <span class="octicon octicon-check"></span>
                              Status premium na 30 dni
                            </li>
                            <li class="list-group-item item-founder check premium">
                              <span class="octicon octicon-check"></span>
                              Certyfikat Założyciela
                            </li>
                            <li class="list-group-item item-tag nocheck gold">
                                <span class="octicon octicon-x"></span>
                               Emblemat Założyciela
                          </li>
                            <li class="list-group-item item-avatar nocheck items">
                              <span class="octicon octicon-x"></span>
                              Awatar i wieniec
                            </li>
                        </ul>
                      </div>
                      <div class="col-sm-6">
                        <ul class="list-group itemlist">
                            <li class="list-group-item item-equipment nocheck house">
                              <span class="octicon octicon-x"></span>
                              Wyposażenie Odkrywcy
                          </li>
                            <li class="list-group-item item-lookingglass nocheck oxe">
                              <span class="octicon octicon-x"></span>
                              Luneta Odkrywcy
                          </li>
                            <li class="list-group-item item-house nocheck horse">
                              <span class="octicon octicon-x"></span>
                              Dom Odkrywcy
                          </li>
                          <li class="list-group-item item-horse nocheck house">
                            <span class="octicon octicon-x"></span>
                            Koń Odkrywcy
                        </li>
                          <li class="list-group-item item-oxe nocheck oxe">
                            <span class="octicon octicon-x"></span>
                            Wół Odkrywcy
                        </li>
                          <li class="list-group-item item-statue nocheck horse">
                            <span class="octicon octicon-x"></span>
                            Posąg Odkrywcy
                        </li>
                        </ul>
                      </div>
                    </div>
                    </div>



                </div>
            </div>
          <!--Sale Content-->
        </div>
        </div>
    </div>
  </div>


  <div class="footer">
    <div class="container">
      <div class="row">
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">O ALBION ONLINE</h6>
              <ul class="footer-menu">
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.compl/home#fighting">WALKA</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/home#economy">EKONOMIA</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/home#housing">DOMOSTWA</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/home#character">POSTAĆ</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/home#world">ŚWIAT</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/home#devices">URZĄDZENIA</a></li>
              </ul>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">FIRMA</h6>
              <ul class="footer-menu">
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/jobs">KARIERA</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/imprint">IMPRESSUM</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/privacy_policy">POLITYKA PRYWATNOŚCI</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/pl/terms_and_conditions">TERMS & CONDITIONS</a></li>
              </ul>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">Follow Albion Online</h6>
              <p>
                  <a href="https://twitter.com/albiononline/"><img class="social-link" src="//assets.albiononline.com/assets/images/social/twitter.jpg?cb=75" alt="Twitter"></a>
                  <a href="https://www.facebook.com/albiononline"><img class="social-link" src="//assets.albiononline.com/assets/images/social/facebook.jpg?cb=75" alt="Facebook"></a>
                  <a href="https://www.youtube.com/channel/UC9gij-VpDFWaaqn1zLM4TEQ"><img class="social-link" src="//assets.albiononline.com/assets/images/social/youtube.jpg?cb=75" alt="Youtube"></a>
                  <a href="http://albiononline.gamepedia.com/Albion_Online_Wiki"><img class="social-link" src="//assets.albiononline.com/assets/images/social/gamepedia.jpg?cb=75" alt="Gamepedia"></a>
                  <a href="https://www.reddit.com/r/albiononline"><img class="social-link" src="//assets.albiononline.com/assets/images/social/reddit.jpg?cb=75" alt="Reddit"></a>
              </p>
          </div>
      </div>
      <div class="row">
          <div class="col-sm-12 medium-12 large-12 columns">
              <p class="text-center text-uppercase"><small>©2015 SANDBOX INTERACTIVE GMBH. ALBION ONLINE JEST ZASTRZEŻONYM ZNAKIEM TOWAROWYM W NIEMCZECH I/LUB W INNYCH KRAJACH. | KONTAKT DLA PRASY: PRESS@ALBIONONLINE.COM</small></p>
          </div>
      </div>
    </div>
  </div>



  <div id="item-beta-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>Dostęp Poziomu 1 do Ostatecznej Bety (rozpoczyna się 1 sierpnia 2016r.)<br />Legendarny dostęp przy wydaniu</p>
    </div>
  </div>

  <div id="item-beta-two-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>Dostęp Poziomu 2 do Ostatecznej Bety (rozpoczyna się 2 sierpnia 2016r.) <br />Epicki dostęp przy wydaniu</p>
    </div>
  </div>

  <div id="item-beta-three-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>Dostęp do Ostatecznej Bety (rozpoczyna się 3 sierpnia 2016r.) <br/ >Dostęp Weterana przy wydaniu</p>
    </div>
  </div>

  <div id="item-gold-content" class="hidden">
    <div>
      <img src="/assets/img/icon_gold.png" alt="Gold Icon">
      <p>Twój początkowy zestaw złota, dzięki któremu zdobędziesz bogactwo i prestiż na ziemiach Albionu.</p>
    </div>
  </div>


  <div id="item-premium-content" class="hidden">
    <div>
      <img src="/assets/img/icon_premium.png" alt="Premium Icon">
      <p>Zyskaj przewagę nad konkurencją dzięki statusowi premium oferującemu szybszy postęp sławy, lepszą prędkość zbierania surowców i wiele innych przywilejów.</p>
    </div>
  </div>

  <div id="item-founder-content" class="hidden">
    <div>
      <img src="/assets/img/icon_founders-certificate.png" alt="Founder Icon">
      <p>Niech wszyscy wiedzą, że jesteś jedną z pierwszych osób, które postawiły stopę w świecie Albionu. Możesz dumnie powiesić Certyfikat na ścianie w Twoim domu i chwalić się nim przed gośćmi!</p>
    </div>
  </div>

  <div id="item-tag-content" class="hidden">
    <div>
      <img src="/assets/img/icon_tag.png" alt="Tag Icon">
      <p>Zaprezentuj swoje upodobanie do pięknych rzeczy poprzez wymyślne pozłacane litery. Ta wyjątkowa czcionka jest widoczna dla wszystkich w świecie gry.</p>
    </div>
  </div>

  <div id="item-avatar-content" class="hidden">
    <div>
      <img src="/assets/img/icon_avatar.png" alt="Avatar Icon">
      <p>Wieniec Założyciela jest idealnym dodatkiem do wyjątkowego emblematu. Szlachetny portret stanowi świetne uzupełnienie awatara. Awatar jest widoczny dla wszystkich w świecie gry i zapewnia Twojej postaci iście królewski urok i czar.</p>
    </div>
  </div>

  <div id="item-equipment-content" class="hidden">
    <div>
      <img src="/assets/img/icon_items.png" alt="Items Icon">
      <p>To zestaw stworzony dla Założycieli. Zawiera nakrycie głowy, tunikę, buty i powiewającą pelerynę (tylko dla Legendarnych Założycieli). Niewielu graczy może dostąpić zaszczytu noszenia go. Chcesz być wśród nich? Przedmiot może zostać zniszczony lub skradziony.</p>
    </div>
  </div>

  <div id="item-lookingglass-content" class="hidden">
    <div>
      <img src="/assets/img/icon_looking-glass.png" alt="Glas Icon">
      <p>Teleskop to obowiązkowy przedmiot nieustraszonego odkrywcy. Umieść w swoim domu i podziwiaj krajobrazy Albionu. Przedmiot może zostać zniszczony lub zagrabiony.</p>
    </div>
  </div>

  <div id="item-house-content" class="hidden">
    <div>
      <img src="/assets/img/icon_house.png" alt="House Icon">
      <p>Ten dom jest dostępny wyłącznie dla Legendarnych Założycieli. Wygląda jak wywrócony do góry dnem okręt i odzwierciedla pasję właściciela do podróży, odkryć i przygód.</p>
    </div>
  </div>

  <div id="item-horse-content" class="hidden">
    <div>
      <img src="/assets/img/icon_horse.png" alt="Horse Icon">
      <p>Wyposażony w lekką zbroję koń pomoże Ci szybciej podróżować przez świat Albion Online. Ten wierzchowiec zwiększa twoje statystyki zbroi oraz udźwig. Przedmiot może zostać zniszczony lub zagrabiony.</p>
    </div>
  </div>

  <div id="item-oxe-content" class="hidden">
    <div>
      <img src="/assets/img/icon_ox.png" alt="Ox Icon">
      <p>Niewiarygodnie potężne zwierzę, które – choć wolne – pomoże ci przewieźć dużo więcej towarów przez świat Albionu. Może to stanowić niezłą przewagę w świecie, gdzie handel oznacza wielkie interesy. Przedmiot może zostać zniszczony lub zagrabiony.</p>
    </div>
  </div>

  <div id="item-statue-content" class="hidden">
    <div>
      <img src="/assets/img/icon_statue.png" alt="Statue Icon">
      <p>Olbrzymi posąg, który możesz umieścić w swojej posiadłości, aby przyciągnąć zazdrosne spojrzenia sąsiadów. Trudno o wspanialsze uczucie! Przedmiot może zostać zniszczony lub zagrabiony.</p>
    </div>
  </div>

    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      var vid = document.getElementById("bgvid");
      vid.addEventListener('ended', function()
      {
      vid.pause();
      // to capture IE10
      vidFade();
      });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.nav a').click(function(){
        var $contentbg = $('.contentbg'),
            newClass = $(this).attr('data-class');

        $contentbg.removeClass('warrior mage thief');
        $contentbg.addClass(newClass);
      });
    });
    </script>

    <script type="text/javascript">
    var popover = function(sel) {
      $("." + sel).popover({
        html: true,
        trigger: 'hover',
        content: function() {
          return $("#" + sel + "-content").html();
        }
      });
    };

    popover("item-beta");
    popover("item-beta-two");
    popover("item-beta-three");
    popover("item-gold");
    popover("item-premium");
    popover("item-founder");
    popover("item-tag");
    popover("item-avatar");
    popover("item-equipment");
    popover("item-lookingglass");
    popover("item-house");
    popover("item-horse");
    popover("item-oxe");
    popover("item-statue");
    </script>
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TV57XR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TV57XR');</script>
<!-- End Google Tag Manager -->
  </body>
</html>
