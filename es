<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Albion Online - Founderpacks</title>
        <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css"/>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'/>
    <link rel="stylesheet" href="/assets/css/theme.css"/>
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-fluid main">
    <div class="background">
      <div class="fullscreen-bg top">
        <video autoplay  poster="/assets/img/image-top-bg.jpg" id="bgvid" loop>
        <source src="/assets/video/bg_low.webm" type="video/webm">
        <source src="/assets/video/bg_low.mp4" type="video/mp4">
        </video>
    </div>
      <img class="imgtop" src="/assets/img/image-top-bg.jpg" alt="Albion Online Landscape Background Top">
    </div>
    <header>
      <a href="https://albiononline.com" class="logo"><img src="/assets/img/logo_black_small.png" alt="Albion Online Logo"></a>
    </header>
    </div>

  <div class="container">
    <div class="row">
       <div class="col-md-12">
         <h1>Conviértete en Fundador</h1>
         <h2>Explora el Nuevo Mundo de Albion</h2>
         <!--Sale Content Tabs-->
          <div class="panel with-nav-tabs panel-albion packs">
             <div class="panel-heading">
                     <ul class="nav nav-tabs">
                       <li class="legendary active"><a href="#tab1default" data-toggle="tab" data-class="mage">Legendario</a></li>
                       <li class="epic"><a href="#tab2default" data-toggle="tab" data-class="warrior">Épico</a></li>
                         <li class="veteran"><a href="#tab3default" data-toggle="tab" data-class="thief">Veterano</a></li>
                     </ul>
             </div>
           </div>
         <!--Sale Content Tabs-->
       </div>
    </div>
  </div>

  <div class="container-fluid packs">
      <div class="row contentbg mage">
        <div class="col-lg-8 col-lg-offset-2 col-md-offset-0">
        <div class="panel with-nav-tabs panel-albion">
          <!--Sale Content-->
            <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane legendary fade active in" id="tab1default">
                    <!--Price Block + Buy Now-->
                    <div class="col-md-3">
                      <div class="priceblock">
                        <div class="icon"><img src="/assets/img/tier-shield-legendary.png" alt="Legendary Tier Shield" /></div>
                        <h3 class="name">Paquete Legendario</h3>
                        <span class="price">$99.95</span>
                      </div>
                    <a class="button" href="https://albiononline.com/es/shop/checkout/begin?product=legendary-founder">¡Comprar ahora!</a>
                  </div>
                  <!--Item Listing-->
                  <div class="col-md-9">
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-beta check premium">
                            <span class="octicon octicon-check"></span>
                            Acceso de nivel 1 a la Beta final
                          </li>
                          <li class="list-group-item item-gold check gold">
                              <span class="octicon octicon-check"></span>
                              12,000 de Oro (unos 50 $)
                        </li>
                          <li class="list-group-item item-premium check items">
                            <span class="octicon octicon-check"></span>
                            90 días de estado premium
                          </li>
                          <li class="list-group-item item-founder check premium">
                            <span class="octicon octicon-check"></span>
                            Certificado de Fundador
                          </li>
                          <li class="list-group-item item-tag check gold">
                              <span class="octicon octicon-check"></span>
                              Identificación dorada de Fundador
                        </li>
                          <li class="list-group-item item-avatar check items">
                            <span class="octicon octicon-check"></span>
                            Avatar y anillo dorados
                          </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-equipment check house">
                            <span class="octicon octicon-check"></span>
                            Equipo de Explorador Legendario
                        </li>
                          <li class="list-group-item item-lookingglass check oxe">
                            <span class="octicon octicon-check"></span>
                            Catalejo de Explorador
                        </li>
                          <li class="list-group-item item-house check horse">
                            <span class="octicon octicon-check"></span>
                            Vivienda de Explorador
                        </li>
                        <li class="list-group-item item-horse check house">
                          <span class="octicon octicon-check"></span>
                          Caballo de Explorador
                      </li>
                        <li class="list-group-item item-oxe check oxe">
                          <span class="octicon octicon-check"></span>
                          Buey de Explorador
                      </li>
                        <li class="list-group-item item-statue check horse">
                          <span class="octicon octicon-check"></span>
                          Estatua de Explorador
                      </li>
                      </ul>
                    </div>
                  </div>


                  </div>
                  <div class="tab-pane epic fade" id="tab2default">
                    <!--Price Block + Buy Now-->
                    <div class="col-md-3">
                      <div class="priceblock">
                        <div class="icon"><img src="/assets/img/tier-shield-epic.png" alt="Epic Tier Shield" /></div>
                        <h3 class="name">Paquete Épico</h3>
                        <span class="price">$49.95</span>
                      </div>
                    <a class="button" href="https://albiononline.com/es/shop/checkout/begin?product=epic-founder">¡Comprar ahora!</a>
                  </div>
                  <!--Item Listing-->
                  <div class="col-md-9">
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-beta-two check premium">
                            <span class="octicon octicon-check"></span>
                            Acceso de nivel 2 a la Beta final
                          </li>
                          <li class="list-group-item item-gold check gold">
                              <span class="octicon octicon-check"></span>
                              4,500 de Oro (unos 20 $)
                        </li>
                          <li class="list-group-item item-premium check items">
                            <span class="octicon octicon-check"></span>
                            60 días de estado premium
                          </li>
                          <li class="list-group-item item-founder check premium">
                            <span class="octicon octicon-check"></span>
                            Certificado de Fundador
                          </li>
                          <li class="list-group-item item-tag check gold">
                              <span class="octicon octicon-check"></span>
                             Identificación plateada de Fundador
                        </li>
                          <li class="list-group-item item-avatar check items">
                            <span class="octicon octicon-check"></span>
                            Avatar y anillo plateados
                          </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul class="list-group itemlist">
                          <li class="list-group-item item-equipment check house">
                            <span class="octicon octicon-check"></span>
                            Equipo de Explorador Épico
                        </li>
                          <li class="list-group-item item-lookingglass check oxe">
                            <span class="octicon octicon-check"></span>
                            Catalejo de Explorador
                        </li>
                          <li class="list-group-item item-house nocheck horse">
                            <span class="octicon octicon-x"></span>
                            Vivienda de Explorador
                        </li>
                        <li class="list-group-item item-horse nocheck house">
                          <span class="octicon octicon-x"></span>
                          Caballo de Explorador
                      </li>
                        <li class="list-group-item item-oxe nocheck oxe">
                          <span class="octicon octicon-x"></span>
                          Buey de Explorador
                      </li>
                        <li class="list-group-item item-statue nocheck horse">
                          <span class="octicon octicon-x"></span>
                          Estatua de Explorador
                      </li>
                      </ul>
                    </div>
                  </div>
                  </div>
                  <div class="tab-pane fade veteran" id="tab3default">
                      <!--Price Block + Buy Now-->
                      <div class="col-md-3">
                        <div class="priceblock">
                          <div class="icon"><img src="/assets/img/tier-shield-veteran.png" alt="Veteran Tier Shield" /></div>
                          <h3 class="name">Paquete Veterano</h3>
                          <span class="price">$29.95</span>
                        </div>
                      <a class="button" href="https://albiononline.com/es/shop/checkout/begin?product=veteran-founder">¡Comprar ahora!</a>
                    </div>
                    <!--Item Listing-->
                    <div class="col-md-9">
                      <div class="col-sm-6">
                        <ul class="list-group itemlist">
                            <li class="list-group-item item-beta-three check premium">
                              <span class="octicon octicon-check"></span>
                              Acceso a la Beta final
                            </li>
                            <li class="list-group-item item-gold check gold">
                                <span class="octicon octicon-check"></span>
                                2,000 de Oro (unos 10 $)
                          </li>
                            <li class="list-group-item item-premium check items">
                              <span class="octicon octicon-check"></span>
                              30 días de estado premium
                            </li>
                            <li class="list-group-item item-founder check premium">
                              <span class="octicon octicon-check"></span>
                              Certificado de Fundador
                            </li>
                            <li class="list-group-item item-tag nocheck gold">
                                <span class="octicon octicon-x"></span>
                                Identificación de Fundador
                          </li>
                            <li class="list-group-item item-avatar nocheck items">
                              <span class="octicon octicon-x"></span>
                              Avatar y anillo
                            </li>
                        </ul>
                      </div>
                      <div class="col-sm-6">
                        <ul class="list-group itemlist">
                            <li class="list-group-item item-equipment nocheck house">
                              <span class="octicon octicon-x"></span>
                              Equipo de Explorador
                          </li>
                            <li class="list-group-item item-lookingglass nocheck oxe">
                              <span class="octicon octicon-x"></span>
                              Catalejo de Explorador
                          </li>
                            <li class="list-group-item item-house nocheck horse">
                              <span class="octicon octicon-x"></span>
                              Vivienda de Explorador
                          </li>
                          <li class="list-group-item item-horse nocheck house">
                            <span class="octicon octicon-x"></span>
                            Caballo de Explorador
                        </li>
                          <li class="list-group-item item-oxe nocheck oxe">
                            <span class="octicon octicon-x"></span>
                           Buey de Explorador
                        </li>
                          <li class="list-group-item item-statue nocheck horse">
                            <span class="octicon octicon-x"></span>
                            Estatua de Explorador
                        </li>
                        </ul>
                      </div>
                    </div>
                    </div>



                </div>
            </div>
          <!--Sale Content-->
        </div>
        </div>
    </div>
  </div>


  <div class="footer">
    <div class="container">
      <div class="row">
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">¿QUÉ ES ALBION ONLINE?</h6>
              <ul class="footer-menu">
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/home#fighting">COMBATES</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/home#economy">ECONOMÍA</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/home#housing">ALOJAMIENTO</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/home#character">PERSONAJE</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/home#world">UNIVERSO</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/home#devices">DISPOSITIVOS</a></li>
              </ul>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">COMPAÑÍA</h6>
              <ul class="footer-menu">
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/jobs">TRABAJOS</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/imprint">EDICIÓN</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/privacy_policy">POLÍTICA DE PRIVACIDAD</a></li>
                  <li class="footer-menu-item"><a class="footer-menu-item-link" href="https://albiononline.com/es/terms_and_conditions">TÉRMINOS Y CONDICIONES</a></li>
              </ul>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4">
              <h6 class="footer-headline">SIGUE A ALBION ONLINE</h6>
              <p>
                  <a href="https://twitter.com/albiononline/"><img class="social-link" src="//assets.albiononline.com/assets/images/social/twitter.jpg?cb=75" alt="Twitter"></a>
                  <a href="https://www.facebook.com/albiononline"><img class="social-link" src="//assets.albiononline.com/assets/images/social/facebook.jpg?cb=75" alt="Facebook"></a>
                  <a href="https://www.youtube.com/channel/UC9gij-VpDFWaaqn1zLM4TEQ"><img class="social-link" src="//assets.albiononline.com/assets/images/social/youtube.jpg?cb=75" alt="Youtube"></a>
                  <a href="http://albiononline.gamepedia.com/Albion_Online_Wiki"><img class="social-link" src="//assets.albiononline.com/assets/images/social/gamepedia.jpg?cb=75" alt="Gamepedia"></a>
                  <a href="https://www.reddit.com/r/albiononline"><img class="social-link" src="//assets.albiononline.com/assets/images/social/reddit.jpg?cb=75" alt="Reddit"></a>
              </p>
          </div>
      </div>
      <div class="row">
          <div class="col-sm-12 medium-12 large-12 columns">
              <p class="text-center text-uppercase"><small>©2015 SANDBOX INTERACTIVE GMBH. ALBION ONLINE ES UNA MARCA REGISTRADA EN ALEMANIA Y OTROS PAÍSES.</small></p>
          </div>
      </div>
    </div>
  </div>



  <div id="item-beta-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>Acceso de nivel 1 a la Beta final (comienza el 1 de agosto de 2016)<br />Acceso legendario en el lanzamiento</p>
    </div>
  </div>

  <div id="item-beta-two-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>Acceso de nivel 2 a la Beta final (comienza el 2 de agosto de 2016)<br />Acceso épico en el lanzamiento</p>
    </div>
  </div>

  <div id="item-beta-three-content" class="hidden">
    <div>
      <img src="/assets/img/icon_level-1-beta.png" alt="Beta Access Icon">
      <p>Acceso a la Beta final (comienza el 3 de agosto de 2016) <br/>Acceso veterano en el lanzamiento</p>
    </div>
  </div>

  <div id="item-gold-content" class="hidden">
    <div>
      <img src="/assets/img/icon_gold.png" alt="Gold Icon">
      <p>Your starter Gold bundle to gain wealth and prestige in the lands of Albion.</p>
    </div>
  </div>


  <div id="item-premium-content" class="hidden">
    <div>
      <img src="/assets/img/icon_premium.png" alt="Premium Icon">
      <p>Adelántate a la competencia con el estado premium; entre muchas otras ventajas, obtendrás fama más rápido y aumentará tu producción de recursos.</p>
    </div>
  </div>

  <div id="item-founder-content" class="hidden">
    <div>
      <img src="/assets/img/icon_founders-certificate.png" alt="Founder Icon">
      <p>¡Que todo el mundo sepa que fuiste de los primeros en poner un pie en el mundo de Albion! Puedes colgar el certificado en la pared de tu vivienda para que lo admiren los visitantes.</p>
    </div>
  </div>

  <div id="item-tag-content" class="hidden">
    <div>
      <img src="/assets/img/icon_tag.png" alt="Tag Icon">
      <p>Demuestra que eres de los que aprecia lo bueno con esta caligrafía chapada en oro. Todo el mundo podrá contemplar esta tipografía tan especial.</p>
    </div>
  </div>

  <div id="item-avatar-content" class="hidden">
    <div>
      <img src="/assets/img/icon_avatar.png" alt="Avatar Icon">
      <p>Un anillo de avatar de Fundador que está en perfecta sintonía con tu placa especial. Y para completar el avatar, un retrato noble. El avatar es visible para todo el mundo y proporciona a tu personaje un encanto y una presencia únicos.</p>
    </div>
  </div>

  <div id="item-equipment-content" class="hidden">
    <div>
      <img src="/assets/img/icon_items.png" alt="Items Icon">
      <p>Este conjunto, formado por sombrero, túnica, botas y una capa ondulante (solo Legendario), es perfecto para Fundadores. Solo unos pocos jugadores podrán tener el privilegio de llevarlo puesto. ¿Serás tú uno de ellos? Este objeto se puede romper y saquear.</p>
    </div>
  </div>

  <div id="item-lookingglass-content" class="hidden">
    <div>
      <img src="/assets/img/icon_looking-glass.png" alt="Glas Icon">
      <p>Este telescopio es imprescindible para cualquier explorador intrépido. Colócalo en tu vivienda y disfruta de las vistas de Albion. Este objeto se puede romper y saquear.</p>
    </div>
  </div>

  <div id="item-house-content" class="hidden">
    <div>
      <img src="/assets/img/icon_house.png" alt="House Icon">
      <p>Esta vivienda está disponible exclusivamente para Fundadores Legendarios. Imita la forma de un gran navío y refleja la pasión de su propietario por la aventura y los descubrimientos.</p>
    </div>
  </div>

  <div id="item-horse-content" class="hidden">
    <div>
      <img src="/assets/img/icon_horse.png" alt="Horse Icon">
      <p>Un caballo equipado con armadura ligera que te ayudará a desplazarte con más velocidad por Albion Online. Esta montura mejora el nivel de tu armadura y tu capacidad para llevar peso. Este objeto se puede romper y saquear.</p>
    </div>
  </div>

  <div id="item-oxe-content" class="hidden">
    <div>
      <img src="/assets/img/icon_ox.png" alt="Ox Icon">
      <p>Una bestia tremendamente poderosa que, aunque es lenta, te permitirá transportar mucha más carga por el mundo de Albion. Desde luego, es una gran ventaja en un mundo donde el comercio es fundamental. Este objeto se puede romper y saquear.</p>
    </div>
  </div>

  <div id="item-statue-content" class="hidden">
    <div>
      <img src="/assets/img/icon_statue.png" alt="Statue Icon">
      <p>Una estatua gigante que puedes colocar en tu parcela en propiedad para despertar la envidia de todos tus vecinos. ¡Es una sensación inigualable! Este objeto se puede romper y saquear.</p>
    </div>
  </div>

    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      var vid = document.getElementById("bgvid");
      vid.addEventListener('ended', function()
      {
      vid.pause();
      // to capture IE10
      vidFade();
      });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      $('.nav a').click(function(){
        var $contentbg = $('.contentbg'),
            newClass = $(this).attr('data-class');

        $contentbg.removeClass('warrior mage thief');
        $contentbg.addClass(newClass);
      });
    });
    </script>

    <script type="text/javascript">
    var popover = function(sel) {
      $("." + sel).popover({
        html: true,
        trigger: 'hover',
        content: function() {
          return $("#" + sel + "-content").html();
        }
      });
    };

    popover("item-beta");
    popover("item-beta-two");
    popover("item-beta-three");
    popover("item-gold");
    popover("item-premium");
    popover("item-founder");
    popover("item-tag");
    popover("item-avatar");
    popover("item-equipment");
    popover("item-lookingglass");
    popover("item-house");
    popover("item-horse");
    popover("item-oxe");
    popover("item-statue");
    </script>
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TV57XR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TV57XR');</script>
<!-- End Google Tag Manager -->
  </body>
</html>
